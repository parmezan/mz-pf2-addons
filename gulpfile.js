const gulp = require('gulp');
const zip = require('gulp-zip');
const clean = require('gulp-clean');

gulp.task('zip', () => {
    return gulp.src([
        "images/**",
        "packs/**",
        "module.json"], {base: ".."})
        .pipe(zip('mz-pf2-addons.zip'))
        .pipe(gulp.src('module.json'))
        .pipe(gulp.dest('dist/'));
});

gulp.task('clean', () => {
    return gulp.src('./dist', {read: false}).pipe(clean());
});