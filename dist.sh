#!/bin/sh

rm -rr dist/*
mkdir -p dist/mz-pf2-addons
cp module.json dist/mz-pf2-addons
cp -r images dist/mz-pf2-addons
cp -r packs dist/mz-pf2-addons
(cd dist && zip -9 -r mz-pf2-addons.zip mz-pf2-addons)



